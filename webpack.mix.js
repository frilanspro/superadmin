const mix = require('laravel-mix');
var $ = require("jquery");
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'node_modules/material-design-iconic-font/dist/css/material-design-iconic-font.min.css',
    'node_modules/animate.css/animate.min.css',
    'node_modules/jquery.scrollbar/jquery.scrollbar.css',
    'node_modules/fullcalendar/dist/fullcalendar.min.css',
], 'public/css/app.css');
mix.js([
    // 'resources/js/app.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/flot/source/jquery.flot.js',
        'node_modules/flot/source/jquery.flot.resize.js',
        'node_modules/jquery.scrollbar/jquery.scrollbar.min.js',
        'node_modules/jquery-scroll-lock/jquery-scrollLock.min.js',
        'node_modules/salvattore/dist/salvattore.min.js',
        'node_modules/flot.curvedlines/curvedLines.js',
        'node_modules/jqvmap/dist/jquery.vmap.min.js',
        'node_modules/jqvmap/dist/maps/jquery.vmap.world.js',
        'node_modules/easy-pie-chart/dist/jquery.easypiechart.min.js',
        'node_modules/peity/jquery.peity.min.js',
        'node_modules/moment/moment.js',
        'node_modules/fullcalendar/dist/fullcalendar.min.js',
    ],
    'public/js/app.js');
